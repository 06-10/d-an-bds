import React from 'react';

export default function Tablehead({data}) {
    return (
        <>
        {data.map((data) => {
            return (
                <td>{data.name}</td>
            )
        })}
        </>
    )
}

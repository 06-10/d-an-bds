import React from 'react'
import Region from '../region'
import {
  Route,
  Routes,
} from "react-router-dom";

export default function Router(){
    return (
        <Routes>
        <Route
          path="/regions"
          element={<Region></Region>}
        ></Route>
      </Routes>
    )
}
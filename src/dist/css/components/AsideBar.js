import React, { useState } from "react";
import data from "./asidebar/json/asidebar.json";

export default function AsideBar() {
  return (
    <>
      <nav className="main-menu">
        <ul>
          {data.map((data) => {
            return (
              <li className="has-subnav">
                <a href={data.link}>
                  <i className={data.icon}></i>
                  <span className="nav-text">{data.name}</span>
                </a>
              </li>
            );
          })}
        </ul>
      </nav>
    </>
  );
}

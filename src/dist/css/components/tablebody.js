import React, { useState , useEffect } from 'react';

export default function Tablebody({datahead,databody}) {
  const [bodyData,setBodyData] = useState();
  useEffect(() => {
    setBodyData(databody);
  },[]);
  console.log(bodyData);
  return (
    <>
    {Object.keys(databody).map((databody) => {
      return (
        <tr>
        {datahead.map((data) => {
          return (
            <td>1.{data.value}</td>
          )
        })}
        </tr>
      )
    })}
    </>
  )
}

import React from "react";
import Table from "./table";
import datahead from "./customerstablehead.json";
import databody from "./customertablebody.json";

export default function region() {
  return (
    <div className="container-jumbotron text-center">
        <div className="row">
            <h1>Bảng danh sách người dùng</h1>
            <Table head={datahead} body={databody} key={Table}></Table>
        </div>
    </div>
  )
}

import React, { useState } from "react";
import Tablebody from "./tablebody";
import Tablehead from "./tablehead";
export default function Table({head,body}) {
    return (
        <table className="table table-striped table-hover">
            <thead>
                <tr>
                    <Tablehead data={head}></Tablehead>
                </tr>
            </thead>
            <tbody>
                <Tablebody databody={body} datahead={head}></Tablebody>
            </tbody>
        </table>
    );
}

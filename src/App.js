import "./App.css";
import AsideBar from "./dist/css/components/AsideBar";
import React from "react";
import Navigation from "./dist/css/components/navigation";
import { BrowserRouter } from "react-router-dom";
import Router from "./dist/css/components/router/router";

function App() {
  return (
    <>
    <BrowserRouter>
      <input id="menu_switch" type="checkbox" />
      <div id="menu" role="navigation">
        <AsideBar></AsideBar>
      </div>
      <div className="page_wrap">
        <Navigation></Navigation>
        <label htmlFor="menu_switch" id="menu_toggle"><i className="fa fa-list fa-2x i-toggle"></i></label>
        <Router></Router>
      </div>
    </BrowserRouter>
    </>
  );
}

export default App;
